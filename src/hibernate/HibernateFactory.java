package hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class HibernateFactory {
	private static SessionFactory sessionFactory;
	private static Session session;
	
	private static SessionFactory configure() {
		Configuration conf = new Configuration().configure();
		sessionFactory = conf.buildSessionFactory();
		return sessionFactory;
	}
	
	public static SessionFactory buildSessionFactory() {
		if (sessionFactory != null) {
			sessionFactory.close();
		}
		return configure();
	}
	
	public static SessionFactory getSessionFactory() {
		if (sessionFactory != null) {
			return sessionFactory;
		}
		return configure();
	}
	
	public static Session openSession() {
		getSessionFactory();
		if (session == null) {
			session = sessionFactory.openSession();
		}
		return session;
	}
	
	public static void closeFactory() {
		if (sessionFactory != null) {
			try {
				sessionFactory.close();
			} catch (HibernateException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void close() {
		if (session != null) {
			try {
				session.close();
				session = null;
			} catch (HibernateException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void rollback(Transaction transaction) {
		if (transaction != null) {
			try {
				transaction.rollback();
			} catch (HibernateException e) {
				e.printStackTrace();
			}
		}
	}
	
}
