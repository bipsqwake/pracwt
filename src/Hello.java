import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import bookstore.Author;
import bookstore.Book;
import bookstore.BookStore;
import bookstore.Customer;
import bookstore.Genre;
import dao.*;
import dao.BookDao.BookFinder;
import dao.CustomerDao.CustomerFinder;
import hibernate.HibernateFactory;
import org.springframework.stereotype.Controller;
public class Hello {
	public static void main(String[] args) {
		BookStore bookstore = new BookStore();
		BookFinder finder = bookstore.getBookDao().getFinder();
		finder.addName("Призраки");
		List<Book> books = finder.find();
		System.out.println(books.size());
		HibernateFactory.close();
		
	}
}
