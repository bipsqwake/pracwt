package bookstore;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Proxy;

import javax.persistence.JoinColumn;

@Entity(name = "books")
@Proxy(lazy = false)
public class Book {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@ManyToOne(fetch = FetchType.EAGER)//, cascade = CascadeType.ALL)
	@JoinColumn(name = "genre")
	private Genre genre;
	@ManyToOne(fetch = FetchType.EAGER)//, cascade = CascadeType.ALL)
	@JoinColumn(name = "publisher")
	private Publisher publisher;
	@Column(name = "year")
	private int year;
	@Column(name = "pages")
	private int pages;
	@ManyToOne(fetch = FetchType.EAGER)//, cascade = CascadeType.ALL)
	@JoinColumn(name = "covertype")
	private CoverType covertype;
	@Column(name = "price")
	private int price;
	@Column(name = "stock")
	private int stock;
	@Column(name = "name")
	private String name;
	@ManyToMany(mappedBy = "books", fetch = FetchType.EAGER)
	/*@JoinTable(name = "authorsattach",
				joinColumns={@JoinColumn(name = "book")},
				inverseJoinColumns={@JoinColumn(name = "author")})*/
	private Set<Author> authors = new HashSet<Author>();
	
	public Book() {
		
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	
	public Genre getGenre() {
		return genre;
	}
	
	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}
	
	public Publisher getPublisher() {
		return publisher;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public int getYear() {
		return year;
	}
	
	public void setPages(int pages) {
		this.pages = pages;
	}
	
	public int getPages() {
		return pages;
	}
	
	public void setCovertype(CoverType covertype) {
		this.covertype = covertype;
	}
	
	public CoverType getCovertype() {
		return covertype;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setStock(int stock) {
		this.stock = stock;
	}
	
	public int getStock() {
		return stock;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}
	
	public Set<Author> getAuthors() {
		return authors;
	}
	
	public void addAuthor(Author author) {
		authors.add(author);
	}
}
