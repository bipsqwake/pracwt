package bookstore;

import java.util.List;

import org.hibernate.HibernateException;

import dao.*;

public class BookStore {
	AuthorDao authorDao = new AuthorDao();
	BookDao bookDao = new BookDao();
	CoverTypeDao coverTypeDao = new CoverTypeDao();
	CustomerDao customerDao = new CustomerDao();
	GenreDao genreDao = new GenreDao();
	OrderConditionDao orderConditionDao = new OrderConditionDao();
	OrderDao orderDao = new OrderDao();
	PublisherDao publisherDao = new PublisherDao();
	RoleDao roleDao = new RoleDao();
	OrderBookDao orderBookDao = new OrderBookDao();

	public AuthorDao getAuthordao() {
		return authorDao;
	}

	public BookDao getBookDao() {
		return bookDao;
	}

	public CoverTypeDao getCoverTypeDao() {
		return coverTypeDao;
	}

	public CustomerDao getCustomerDao() {
		return customerDao;
	}

	public GenreDao getGenreDao() {
		return genreDao;
	}

	public OrderConditionDao getOrderConditionDao() {
		return orderConditionDao;
	}

	public OrderDao getOrderDao() {
		return orderDao;
	}

	public PublisherDao getPublisherDao() {
		return publisherDao;
	}

	public RoleDao getRoleDao() {
		return roleDao;
	}
	public OrderBookDao getOrderBookDao() {
		return orderBookDao;
	}
	
	
	
}
