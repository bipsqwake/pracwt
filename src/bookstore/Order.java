package bookstore;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Proxy;

@Entity(name = "ordersattach")
@Proxy(lazy = false)
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@ManyToOne
	@JoinColumn(name = "customer")
	private Customer customer;
	@ManyToOne
	@JoinColumn(name = "cond")
	private OrderCondition cond;
	@Column(name = "time") 
	private Date time;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "pk.order", cascade=CascadeType.ALL)
	private Set<OrderBook> orderBooks = new HashSet<OrderBook>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public OrderCondition getCond() {
		return cond;
	}
	public void setCond(OrderCondition cond) {
		this.cond = cond;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public Set<OrderBook> getOrderBooks() {
		return orderBooks;
	}
	public void setOrderBooks(Set<OrderBook> orderBooks) {
		this.orderBooks = orderBooks;
	}
	public void addOrderBook(OrderBook orderBook) {
		orderBooks.add(orderBook);
	}
	
}
