package bookstore;

import java.beans.Transient;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;

import org.hibernate.annotations.Proxy;

@Entity(name = "orders")
@Proxy(lazy = false)
@AssociationOverrides({
	@AssociationOverride(name = "pk.ordersattach",
		joinColumns = @JoinColumn(name = "id")),
	@AssociationOverride(name = "pk.books",
		joinColumns = @JoinColumn(name = "book"))})
public class OrderBook {
	@EmbeddedId
	private OrderBookId pk = new OrderBookId();
	@Column(name = "num")
	private int num;
	
	public void setPk(OrderBookId pk) {
		this.pk = pk;
	}
	
	public OrderBookId getPk() {
		return pk;
	}
	
	public void setOrder(Order order) {
		getPk().setOrder(order);
	}
	@Transient
	public Order getOrder() {
		return getPk().getOrder();
	}
	
	public void setBook(Book book) {
		getPk().setBook(book);
	}
	
	@Transient
	public Book getBook() {
		return getPk().getBook();
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
	
	
}
