package bookstore;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.Proxy;

@Entity(name = "authors")
@Proxy(lazy = false)
public class Author {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	private int id;
	@Column(name = "name")
	private String name;
	@ManyToMany(fetch = FetchType.EAGER)//(cascade = CascadeType.ALL)
	@JoinTable(name = "authorsattach",
				joinColumns={@JoinColumn(name = "author")},
				inverseJoinColumns={@JoinColumn(name = "book")})
	private Set<Book> books = new HashSet<Book>();
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setBooks(Set<Book> books) {
		this.books = books;
	}
	
	public Set<Book> getBooks() {
		return books;
	}
	
	public void addBook(Book book) {
		books.add(book);
	}
	
	public void removeBook(Book book) {
		if (books.contains(book))
			books.remove(book);
	}

}
