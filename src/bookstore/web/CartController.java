package bookstore.web;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import bookstore.*;
import dao.CustomerDao.CustomerFinder;

@Controller
public class CartController {
	BookStore bookstore = new BookStore();
	
	@RequestMapping(value="/cart.do", method = RequestMethod.GET)
	public ModelAndView get(@RequestParam("userId") int userId) {
		ModelAndView mav = new ModelAndView();
		Customer customer = bookstore.getCustomerDao().find(userId);
		mav.addObject("user", customer);
		Order order = null;
		for (Order a: customer.getOrders()) {
			if (a.getCond().getName().equals("assemble")) {
				order = a;
			}
		}
		if  (order != null && order.getOrderBooks().size() > 0){
			mav.addObject("order", order);
			mav.addObject("orderbookslist", order.getOrderBooks());
			int sum = 0;
			for (OrderBook a: order.getOrderBooks()) {
				sum += a.getBook().getPrice() * a.getNum();
			}
			mav.addObject("sum", sum);
		}
		Set<Order> allOrders = customer.getOrders();
		List<Order> procOrders = new LinkedList<>();
		List<Order> prepOrders = new LinkedList<>();
		List<Order> compOrders = new LinkedList<>();
		for (Order a: allOrders) {
			switch (a.getCond().getName()) {
			case "processing":
				procOrders.add(a);
				break;
			case "prepared":
				prepOrders.add(a);
				break;
			case "completed":
				compOrders.add(a);
				break;
			}
		}
		mav.addObject("procOrders", procOrders);
		mav.addObject("prepOrders", prepOrders);
		mav.addObject("compOrders", compOrders);
		mav.setViewName("cart");
		return mav;
	}
	
	@RequestMapping(value="checkout.do", method = RequestMethod.POST)
	public String post(@RequestParam("userId") int userId) {
		Customer customer = bookstore.getCustomerDao().find(userId);
		Order order = null;
		for (Order a: customer.getOrders()) {
			if (a.getCond().getName().equals("assemble")) {
				order = a;
			}
		}
		if (order == null) {
			//TODO unknown error
			//return
		}
		order.setCond(bookstore.getOrderConditionDao().getByName("processing"));
		return "redirect:cart.do?userId=" + customer.getId();
	}
	
	@RequestMapping(value="cancelorder.do", method = RequestMethod.POST)
	public String cancel(@RequestParam("userId") int userId) {
		Customer customer = bookstore.getCustomerDao().find(userId);
		Order order = null;
		for (Order a: customer.getOrders()) {
			if (a.getCond().getName().equals("assemble")) {
				order = a;
			}
		}
		for (OrderBook a: order.getOrderBooks()) {
			a.getBook().setStock(a.getBook().getStock() + a.getNum());
			bookstore.getBookDao().saveOrUpdate(a.getBook());
		}
		customer.getOrders().remove(order);
		bookstore.getOrderDao().delete(order);
		return "redirect:cart.do?userId=" + customer.getId();
	}
	
	@RequestMapping(value="removebook.do", method = RequestMethod.POST)
	public String removeBook(@RequestParam("userId") int userId,
							@RequestParam("bookId") int bookId) {
		Customer customer = bookstore.getCustomerDao().find(userId);
		Order order = null;
		for (Order a: customer.getOrders()) {
			if (a.getCond().getName().equals("assemble")) {
				order = a;
			}
		}
		//TODO unknown error
		OrderBook orderbook = null;
		for (OrderBook a: order.getOrderBooks()) {
			if (a.getBook().getId() == bookId) {
				orderbook = a;
			}
		}
		Book book = bookstore.getBookDao().find(bookId);
		book.setStock(book.getStock() + 1);
		if (orderbook.getNum() > 1) {
			orderbook.setNum(orderbook.getNum() - 1);
			bookstore.getOrderBookDao().saveOrUpdate(orderbook);
		} else {
			order.getOrderBooks().remove(orderbook);
			bookstore.getOrderDao().saveOrUpdate(order);
			bookstore.getOrderBookDao().delete(orderbook);
		}
		return "redirect:cart.do?userId=" + customer.getId();
	}

}
