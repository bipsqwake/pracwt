package bookstore.web;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import bookstore.*;
import dao.CustomerDao.CustomerFinder;

@Controller
@RequestMapping("/signup.do")
public class SignupController {
	BookStore bookstore = new BookStore();
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView get() {
		ModelAndView mav = new ModelAndView();
		mav.addObject("user", new Customer());
		mav.setViewName("signup");
		return mav;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView post(Customer user) {
		ModelAndView mav = new ModelAndView();
		if (user.getFirstname().length() == 0) {
			mav.addObject("errorMes", "Firstname can't be empty");
			mav.setViewName("signup");
			mav.addObject("user", user);
			return mav;
		}
		if (user.getSecondname().length() == 0) {
			mav.addObject("errorMes", "Secondname can't be empty");
			mav.setViewName("signup");
			mav.addObject("user", user);
			return mav;
		}
		if (user.getLogin().length() == 0) {
			mav.addObject("errorMes", "Login can't be empty");
			mav.setViewName("signup");
			mav.addObject("user", user);
			return mav;
		}
		if (user.getPassword().length() == 0) {
			mav.addObject("errorMes", "Password can't be empty");
			mav.setViewName("signup");
			mav.addObject("user", user);
			return mav;
		}
		if (user.getEmail().length() == 0) {
			mav.addObject("errorMes", "Email can't be empty");
			mav.setViewName("signup");
			mav.addObject("user", user);
			return mav;
		}
		if (user.getPhone().length() == 0) {
			mav.addObject("errorMes", "Phone can't be empty");
			mav.setViewName("signup");
			mav.addObject("user", user);
			return mav;
		}
		if (user.getAddress().length() == 0) {
			mav.addObject("errorMes", "Address can't be empty");
			mav.setViewName("signup");
			mav.addObject("user", user);
			return mav;
		}
		CustomerFinder finder = bookstore.getCustomerDao().getFinder();
		List<Customer> customers = finder.setLogin(user.getLogin()).find();
		if (customers.size() != 0) {
			mav.addObject("errorMes", "That username already registered");
			mav.setViewName("signup");
			mav.addObject("user", user);
			return mav;
		}
		CustomerFinder finder2 = bookstore.getCustomerDao().getFinder();
		customers = finder2.setEmail(user.getEmail()).find();
		if (customers.size() != 0) {
			mav.addObject("errorMes", "That email occupied");
			mav.setViewName("signup");
			mav.addObject("user", user);
			return mav;
		}
		CustomerFinder finder3 = bookstore.getCustomerDao().getFinder();
		customers = finder3.setPhone(user.getPhone()).find();
		if (customers.size() != 0) {
			mav.addObject("errorMes", "That phone occupied");
			mav.setViewName("signup");
			mav.addObject("user", user);
			return mav;
		}
		user.setRole(bookstore.getRoleDao().getByName("user"));
		bookstore.getCustomerDao().saveOrUpdate(user);
		mav.setViewName("loginred");
		mav.addObject("mes", "Sucssesfully signuped");
		return mav;
	}

}
