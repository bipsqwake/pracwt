package bookstore.web;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import bookstore.*;
import dao.CustomerDao.CustomerFinder;

@Controller
@RequestMapping("/login.do")
public class LoginController {
	BookStore bookstore = new BookStore();
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView get(@RequestParam("mes") String mes) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("user", new Customer());
		mav.addObject("correctSignUpMes", mes);
		mav.setViewName("login");
		
		return mav;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String post(@ModelAttribute("user")Customer user, Model model) {
		CustomerFinder finder = bookstore.getCustomerDao().getFinder();
		List<Customer> customers = finder.setLogin(user.getLogin()).find();
		if (customers.isEmpty() || customers.size() > 1) {
			model.addAttribute("errorMes", "No such user");
			return "login";
		}
		Customer customer = customers.get(0);
		if (!customer.getPassword().equals(user.getPassword())) {
			model.addAttribute("errorMes", "Wrong login or password");
			return "login";
		}
		if (customer.getRole().getName().equals("user"))
			return "redirect:/user.do?userId=" + customer.getId();
		else return "redirect:/admin.do?userId=" + customer.getId();
	}

}
