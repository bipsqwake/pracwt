package bookstore.web;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import bookstore.*;
import dao.BookDao.BookFinder;

@Controller
public class UserController {
	BookStore bookstore = new BookStore();
	
	@RequestMapping(value = "user.do", method = RequestMethod.GET)
	public ModelAndView get(@RequestParam("userId") int userId) {
		ModelAndView mav = new ModelAndView();
		Customer customer = bookstore.getCustomerDao().find(userId);
		Order order = null;
		int booksnum = 0;
		for (Order a: customer.getOrders()) {
			if (a.getCond().getName().equals("assemble")) {
				order = a;
			}
		}
		if (order != null) {
			for (OrderBook a: order.getOrderBooks()) {
				booksnum += a.getNum();
			}
		}
		mav.addObject("booksnum", booksnum);
		List<Book> books = bookstore.getBookDao().findAll();
		mav.addObject("bookList", books);
		mav.addObject("user", customer);
		mav.setViewName("user");
		return mav;
	}
	
	@RequestMapping(value="usersearch.do", method = RequestMethod.POST)
	public ModelAndView search(@RequestParam("name") String name,
			@RequestParam("publisher") String publisher,
			@RequestParam("genre") String genre,
			@RequestParam("maxPrice") String maxPrice,
			@RequestParam("minPrice") String minPrice,
			@RequestParam("userId") int userId) {
		ModelAndView modelAndView = new ModelAndView();
		BookFinder finder = bookstore.getBookDao().getFinder();
		if (minPrice.matches("[0-9]+")) {
			finder.setMinPrice(Integer.parseInt(minPrice));
		}
		if (maxPrice.matches("[0-9]+")) {
			finder.setMaxPrice(Integer.parseInt(maxPrice));
		}
		if (!genre.equals("")) {
			finder.addGenre(genre);
		}
		if (!publisher.equals("")) {
			finder.addPublisher(publisher);
		}
		List<Book> books = finder.find();
		modelAndView.addObject("bookList", books);
        modelAndView.setViewName("user");
        modelAndView.addObject("user", bookstore.getCustomerDao().find(userId));
        return modelAndView;
	}
	
	
	@RequestMapping(value = "addtocart.do")
	public String post(@RequestParam("bookId") int bookId, @RequestParam("userId") int userId) {
		Customer customer = bookstore.getCustomerDao().find(userId);
		Set<Order> orders = customer.getOrders();
		Order order = null;
		for (Order a: orders) {
			if (a.getCond().getName().equals("assemble")) {
				order = a;
			}
		}
		if (order == null) {
			order = new Order();
			order.setCond(bookstore.getOrderConditionDao().getByName("assemble"));
			order.setCustomer(customer);
			Calendar cal = new GregorianCalendar();
			order.setTime(cal.getTime());
			customer.addOrder(order);
			bookstore.getOrderDao().saveOrUpdate(order);
		}
		Book book = bookstore.getBookDao().find(bookId);
		if (book.getStock() <= 0) {
			return "redirect:user.do?userId=" + customer.getId();
		}
		for (OrderBook a : order.getOrderBooks()) {
			if (a.getBook().getId() == book.getId()) {
				a.setNum(a.getNum() + 1);
				a.getBook().setStock(a.getBook().getStock() - 1);
				bookstore.getOrderBookDao().saveOrUpdate(a);
				bookstore.getBookDao().saveOrUpdate(a.getBook());
				return "redirect:user.do?userId=" + customer.getId();
			}
		}
		OrderBook orderbook = new OrderBook();
		orderbook.setBook(book);
		book.setStock(book.getStock() - 1);
		orderbook.setNum(1);
		orderbook.setOrder(order);
		order.addOrderBook(orderbook);
		bookstore.getBookDao().saveOrUpdate(book);
		bookstore.getOrderBookDao().saveOrUpdate(orderbook);
		return "redirect:user.do?userId=" + customer.getId();
	}
	
	
}
