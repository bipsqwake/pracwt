package bookstore.web;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import bookstore.*;
import dao.BookDao.BookFinder;
import dao.CustomerDao.CustomerFinder;
import hibernate.HibernateFactory;

@Controller
public class AdminController {
	BookStore bookstore = new BookStore();
	
	@RequestMapping(value="/admin.do", method = RequestMethod.GET)
	public ModelAndView get(@RequestParam("userId") int userId) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("admin");
		mav.addObject("user", bookstore.getCustomerDao().find(userId));
		return mav;
		
	}
	
	@RequestMapping(value="/bookcontrol.do", method = RequestMethod.GET)
	public ModelAndView bookcontrol(@RequestParam("userId") int userId) {
		ModelAndView modelAndView = new ModelAndView();
        List<Book> books = bookstore.getBookDao().findAll();
        modelAndView.addObject("bookList", books);
        Customer user = bookstore.getCustomerDao().find(userId);
        modelAndView.addObject("user", user);
        modelAndView.setViewName("bookcontrol");
        return modelAndView;
	}
	
	@RequestMapping(value="/editbook.do", method = RequestMethod.GET)
	public ModelAndView editbook(@RequestParam("bookId") int bookId, @RequestParam("userId") int userId) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("bookedit");
		Book book = null;
		if (bookId == 0) {
			book = new Book();
		} else {
			book = bookstore.getBookDao().find(bookId);
		}	
		Customer user = bookstore.getCustomerDao().find(userId);
		mav.addObject("user", user);
		mav.addObject("book", book);
		return mav;
	}
	
	@RequestMapping(value="/savebook.do", method=RequestMethod.POST)
	public String postEditBook(@RequestParam("bookId") int bookId, @RequestParam("userId") int userId, @ModelAttribute Book book) {
		Book toUpdate = null;
		if (bookId == 0) {
			toUpdate = new Book();
		} else {
			toUpdate = bookstore.getBookDao().find(bookId);
		}
		Publisher publisher = bookstore.getPublisherDao().getByName(book.getPublisher().getName());
		if (publisher != null) {
			toUpdate.setPublisher(publisher);
		} else {
			Publisher newPublisher = new Publisher();
			newPublisher.setName(book.getPublisher().getName());
			bookstore.getPublisherDao().saveOrUpdate(newPublisher);
			toUpdate.setPublisher(newPublisher);
		}
		Genre genre = bookstore.getGenreDao().getByName(book.getGenre().getName());
		if (genre != null) {
			toUpdate.setGenre(genre);
		} else {
			genre = new Genre();
			genre.setName(book.getGenre().getName());
			bookstore.getGenreDao().saveOrUpdate(genre);
			toUpdate.setGenre(genre);
		}
		CoverType covertype = bookstore.getCoverTypeDao().getByName(book.getCovertype().getName());
		if (covertype != null) {
			toUpdate.setCovertype(covertype);
		} else {
			covertype = new CoverType();
			covertype.setName(book.getCovertype().getName());
			bookstore.getCoverTypeDao().saveOrUpdate(covertype);
			toUpdate.setCovertype(covertype);
		}
		toUpdate.setName(book.getName());
		toUpdate.setPages(book.getPages());
		toUpdate.setPrice(book.getPrice());
		toUpdate.setStock(book.getStock());
		toUpdate.setYear(book.getYear());
		System.out.println(toUpdate.getCovertype().getName());
		bookstore.getBookDao().saveOrUpdate(toUpdate);
		return "redirect:bookcontrol.do?userId=" + userId;
	}
	
	@RequestMapping(value="removeauthor.do")
	public String removeAuthor(@RequestParam("bookId") int bookId,
								@RequestParam("authId") int authId,
								@RequestParam("userId") int userId) {
		Book book = bookstore.getBookDao().find(bookId);
		Author author = bookstore.getAuthordao().find(authId);
		book.getAuthors().remove(author);
		bookstore.getAuthordao().find(authId).getBooks().remove(book);
		bookstore.getBookDao().saveOrUpdate(book);
		System.out.println(bookstore.getBookDao().find(bookId).getAuthors().size());
		return "redirect:editbook.do?bookId=" + bookId + "&userId=" + userId;
	}
	
	@RequestMapping(value="editbook.do", method = RequestMethod.POST, params = { "add" })
	public String addAuthor(@RequestParam("authorname") String authorname,
							@RequestParam("userId") int userId,
							@RequestParam("bookId") int bookId) {
		Author author = bookstore.getAuthordao().getByName(authorname);
		if (author == null) {
			author = new Author();
			author.setName(authorname);
			bookstore.getAuthordao().saveOrUpdate(author);
			author = bookstore.getAuthordao().getByName(authorname);
		}
		Book book = bookstore.getBookDao().find(bookId);
		book.addAuthor(author);
		author.addBook(book);
		bookstore.getBookDao().saveOrUpdate(book);
		return "redirect:editbook.do?bookId=" + bookId + "&userId=" + userId;
	}
	
	@RequestMapping(value="deletebook.do", method = RequestMethod.GET)
	public String deletebook(@RequestParam("bookId") int bookId,
								@RequestParam("userId") int userId) {
		Book book = bookstore.getBookDao().find(bookId);
		for (Order a: (List<Order>)bookstore.getOrderDao().findAll()) {
			OrderBook toDel = null;
			for (OrderBook b: a.getOrderBooks()) {
				if (b.getBook().getId() == bookId) {
					toDel = b;
				}
			}
			if (toDel != null) {
				a.getOrderBooks().remove(toDel);
				bookstore.getOrderBookDao().delete(toDel);
			}
		}
		bookstore.getBookDao().delete(book);
		return "redirect:bookcontrol.do?userId=" + userId;
	}
	
	@RequestMapping(value="usercontrol.do", method = RequestMethod.GET)
	public ModelAndView userControl(@RequestParam("userId") int userId) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("usercontrol");
		List<Customer> users = bookstore.getCustomerDao().findAll();
		mav.addObject("users", users);
		mav.addObject("admin", bookstore.getCustomerDao().find(userId));
		
		return mav;
	}
	
	@RequestMapping(value="makeadmin.do", method = RequestMethod.POST)
	public String makeAdmin(@RequestParam("userId") int userId,
							@RequestParam("adminId") int adminId) {
		Customer customer = bookstore.getCustomerDao().find(userId);
		customer.setRole(bookstore.getRoleDao().getByName("administrator"));
		bookstore.getCustomerDao().saveOrUpdate(customer);
		return "redirect:usercontrol.do?userId=" + adminId;
	}
	
	@RequestMapping(value="disadmin.do", method = RequestMethod.POST)
	public String disAdmin(@RequestParam("userId") int userId,
							@RequestParam("adminId") int adminId,
							Model model) {
		if (userId == adminId) {
			model.addAttribute("errorMes", "Cant disadmin yourself");
			return "redirect:usercontrol.do?userId=" + adminId;
		}
		Customer customer = bookstore.getCustomerDao().find(userId);
		customer.setRole(bookstore.getRoleDao().getByName("user"));
		bookstore.getCustomerDao().saveOrUpdate(customer);
		return "redirect:usercontrol.do?userId=" + adminId;
	}
	
	@RequestMapping(value="ordercontrol.do")
	public ModelAndView ordercontrol(@RequestParam("userId")int userId) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("ordercontrol");
		List<Order> orders = bookstore.getOrderDao().findAll();
		mav.addObject("orderList", orders);
		Customer customer = bookstore.getCustomerDao().find(userId);
		mav.addObject("user", customer);
		return mav;
	}
	
	@RequestMapping(value="prepare.do", method = RequestMethod.POST)
	public String prepare(@RequestParam("orderId") int orderId,
							@RequestParam("userId") int userId) {
		Order order = bookstore.getOrderDao().find(orderId);
		order.setCond(bookstore.getOrderConditionDao().getByName("prepared"));
		bookstore.getOrderDao().saveOrUpdate(order);
		return "redirect:ordercontrol.do?userId=" + userId;
	}
	
	@RequestMapping(value="complete.do", method = RequestMethod.POST)
	public String complete(@RequestParam("orderId") int orderId,
							@RequestParam("userId") int userId) {
		Order order = bookstore.getOrderDao().find(orderId);
		order.setCond(bookstore.getOrderConditionDao().getByName("completed"));
		bookstore.getOrderDao().saveOrUpdate(order);
		return "redirect:ordercontrol.do?userId=" + userId;
	}
	
	@RequestMapping(value="cancel.do", method = RequestMethod.POST)
	public String cancel(@RequestParam("orderId") int orderId,
							@RequestParam("userId") int userId) {
		Order order = bookstore.getOrderDao().find(orderId);
		for (OrderBook a: order.getOrderBooks()) {
			a.getBook().setStock(a.getBook().getStock() + a.getNum());
			bookstore.getBookDao().saveOrUpdate(a.getBook());
		}
		bookstore.getOrderDao().delete(order);
		return "redirect:ordercontrol.do?userId=" + userId;
	}
	
	@RequestMapping(value="remove.do", method = RequestMethod.POST)
	public String remove(@RequestParam("orderId") int orderId,
							@RequestParam("userId") int userId) {
		Order order = bookstore.getOrderDao().find(orderId);
		bookstore.getOrderDao().delete(order);
		return "redirect:ordercontrol.do?userId=" + userId;
	}
	
	@RequestMapping(value="show.do", method = RequestMethod.POST)
	public ModelAndView show(@RequestParam("userId") int userId,
										@RequestParam("condition") String condition) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("ordercontrol");
		List<Order> allorders = bookstore.getOrderDao().findAll();
		List<Order> orders = new LinkedList<>();
		if (condition.equals("all")) {
			orders = allorders;
		} else {
			for (Order a: allorders) {
				if (a.getCond().getName().equals(condition))
					orders.add(a);
			}
		}
		mav.addObject("orderList", orders);
		Customer customer = bookstore.getCustomerDao().find(userId);
		mav.addObject("user", customer);
		return mav;
	}
	
	@RequestMapping(value="search.do", method = RequestMethod.POST)
	public ModelAndView search(@RequestParam("userId") int userId,
							@RequestParam("search") String search) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("ordercontrol");
		List<Order> allorders = bookstore.getOrderDao().findAll();
		List<Order> orders = new LinkedList<>();
		for (Order a: allorders) {
			Integer id = a.getId();
			if (id.toString().equals(search)) {
				orders.add(a);
			}
			if (a.getCustomer().getFirstname().equals(search)
				|| a.getCustomer().getSecondname().equals(search)
				|| a.getCustomer().getLogin().equals(search)) {
				orders.add(a);
			}
		}
		mav.addObject("orderList", orders);
		Customer customer = bookstore.getCustomerDao().find(userId);
		mav.addObject("user", customer);
		return mav;
	}
	
	@RequestMapping(value="adminsearch.do", method = RequestMethod.POST)
	public ModelAndView search(@RequestParam("name") String name,
			@RequestParam("publisher") String publisher,
			@RequestParam("genre") String genre,
			@RequestParam("maxPrice") String maxPrice,
			@RequestParam("minPrice") String minPrice,
			@RequestParam("userId") int userId) {
		ModelAndView modelAndView = new ModelAndView();
		BookFinder finder = bookstore.getBookDao().getFinder();
		if (minPrice.matches("[0-9]+")) {
			finder.setMinPrice(Integer.parseInt(minPrice));
		}
		if (maxPrice.matches("[0-9]+")) {
			finder.setMaxPrice(Integer.parseInt(maxPrice));
		}
		if (!genre.equals("")) {
			finder.addGenre(genre);
		}
		if (!publisher.equals("")) {
			finder.addPublisher(publisher);
		}
		List<Book> books = finder.find();
		modelAndView.addObject("bookList", books);
		modelAndView.addObject("user", bookstore.getCustomerDao().find(userId));
        modelAndView.setViewName("bookcontrol");
        return modelAndView;
	}
}
