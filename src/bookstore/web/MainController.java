package bookstore.web;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import bookstore.*;
import dao.BookDao.BookFinder;
import hibernate.HibernateFactory;

@Controller
public class MainController { 
	BookStore bookstore = new BookStore();
	
	@RequestMapping(value="welcome.do", method = RequestMethod.GET)
    public ModelAndView first() {
        ModelAndView modelAndView = new ModelAndView();
        StringBuilder b = new StringBuilder();
        List<Book> books = bookstore.getBookDao().findAll();
        modelAndView.addObject("bookList", books);
        modelAndView.addObject("book", new Book());
        modelAndView.setViewName("welcome");
        return modelAndView;
    }
	
	@RequestMapping(value="welcomesearch.do", method = RequestMethod.POST)
	public ModelAndView search(@RequestParam("name") String name,
			@RequestParam("publisher") String publisher,
			@RequestParam("genre") String genre,
			@RequestParam("maxPrice") String maxPrice,
			@RequestParam("minPrice") String minPrice) {
		ModelAndView modelAndView = new ModelAndView();
		BookFinder finder = bookstore.getBookDao().getFinder();
		if (minPrice.matches("[0-9]+")) {
			finder.setMinPrice(Integer.parseInt(minPrice));
		}
		if (maxPrice.matches("[0-9]+")) {
			finder.setMaxPrice(Integer.parseInt(maxPrice));
		}
		if (!genre.equals("")) {
			finder.addGenre(genre);
		}
		if (!publisher.equals("")) {
			finder.addPublisher(publisher);
		}
		if (!name.equals("")) {
			finder.addName(name);
		}
		List<Book> books = finder.find();
		modelAndView.addObject("bookList", books);
        modelAndView.setViewName("welcome");
        return modelAndView;
	}
}