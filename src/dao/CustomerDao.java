package dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import bookstore.Role;
import bookstore.Customer;

public class CustomerDao extends AbstractDao {
	public CustomerDao() {
		super();
	}
	
	public void saveOrUpdate(Customer customer) {
		super.saveOrUpdate(customer);
	}
	
	public void delete(Customer customer) {
		super.delete(customer);
	}
	
	public Customer find(int id) {
		return (Customer) super.find(Customer.class, id);
	}
	
	public List findAll() {
		return super.findAll("customers");
	}
	
	public CustomerFinder getFinder() {
		return new CustomerFinder();
	}
	
	public class CustomerFinder {
		private String firstname;
		private String secondname;
		private String email;
		private String phone;
		private String address;
		private String login;
		private String role;
		private CriteriaBuilder builder;
		Root<Customer> from;
		public CustomerFinder setFirstname(String firstname) {
			this.firstname = firstname;
			return this;
		}
		public CustomerFinder setSecondname(String secondname) {
			this.secondname = secondname;
			return this;
		}
		public CustomerFinder setEmail(String email) {
			this.email = email;
			return this;
		}
		public CustomerFinder setPhone(String phone) {
			this.phone = phone;
			return this;
		}
		public CustomerFinder setAddress(String address) {
			this.address = address;
			return this;
		}
		public CustomerFinder setLogin(String login) {
			this.login = login;
			return this;
		}
		public CustomerFinder setRole(String role) {
			this.role = role;
			return this;
		}
		public List<Customer> find() {
			start();
			builder = session.getCriteriaBuilder();
			CriteriaQuery<Customer> customerQuery = builder.createQuery(Customer.class);
			from = customerQuery.from(Customer.class);
			Join<Customer, Role> roleJoin = from.join("role");
			Predicate pred = builder.and();
			pred = addPred(pred, "firstname", firstname);
			pred = addPred(pred, "secondname", secondname);
			pred = addPred(pred, "email", email);
			pred = addPred(pred, "phone", phone);
			pred = addPred(pred, "address", address);
			pred = addPred(pred, "login", login);
			if (role != null) {
				pred = builder.and(pred, builder.equal(roleJoin.get("name"), role));
			}
			customerQuery.select(from).where(pred);
			TypedQuery<Customer> tq = session.createQuery(customerQuery);
			transaction.commit();
			//closeSession();
			return tq.getResultList();
		}
		private Predicate addPred(Predicate pred, String column, String value) {
			if (value != null) {
				pred = builder.and(pred, builder.like(from.get(column), value));
			}
			return pred;
		}
	}
}
