package dao;

import java.util.List;

import bookstore.CoverType;
import bookstore.Publisher;

public class PublisherDao extends AbstractDao {
	public PublisherDao() {
		super();
	}
	
	public void saveOrUpdate(Publisher publisher) {
		super.saveOrUpdate(publisher);
	}
	
	public void delete(Publisher publisher) {
		super.delete(publisher);
	}
	
	public Publisher find(int id) {
		return (Publisher) super.find(Publisher.class, id);
	}
	
	public List findAll() {
		return super.findAll("publishers");
	}
	
	public Publisher getByName(String name) {
		List<Publisher> all = findAll();
		for (Publisher a: all) {
			if (a.getName().equals(name)) {
				return a;
			}
		}
		return null;
	}
}
