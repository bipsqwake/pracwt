package dao;

import java.util.List;

import bookstore.CoverType;

public class CoverTypeDao extends AbstractDao {
	public CoverTypeDao() {
		super();
	}
	
	public void saveOrUpdate(CoverType coverType) {
		super.saveOrUpdate(coverType);
	}
	
	public void delete(CoverType coverType) {
		super.delete(coverType);
	}
	
	public CoverType find(int id) {
		return (CoverType) super.find(CoverType.class, id);
	}
	
	public List findAll() {
		return super.findAll("covertype");
	}
	
	public CoverType getByName(String name) {
		List<CoverType> all = findAll();
		for (CoverType a: all) {
			if (a.getName().equals(name)) {
				return a;
			}
		}
		return null;
	}
}
