package dao;

import java.util.List;

import bookstore.CoverType;
import bookstore.OrderCondition;

public class OrderConditionDao extends AbstractDao {
	public OrderConditionDao() {
		super();
	}
	
	public void saveOrUpdate(OrderCondition orderCondition) {
		super.saveOrUpdate(orderCondition);
	}
	
	public void delete(OrderCondition orderCondition) {
		super.delete(orderCondition);
	}
	
	public OrderCondition find(int id) {
		return (OrderCondition) super.find(OrderCondition.class, id);
	}
	
	public List findAll() {
		return super.findAll("orderscond");
	}
	
	public OrderCondition getByName(String name) {
		List<OrderCondition> all = findAll();
		for (OrderCondition a: all) {
			if (a.getName().equals(name)) {
				return a;
			}
		}
		return null;
	}
}
