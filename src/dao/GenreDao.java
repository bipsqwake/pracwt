package dao;

import java.util.List;

import bookstore.CoverType;
import bookstore.Genre;

public class GenreDao extends AbstractDao {
	public GenreDao() {
		super();
	}
	
	public void saveOrUpdate(Genre genre) {
		super.saveOrUpdate(genre);
	}
	
	public void delete(Genre genre) {
		super.delete(genre);
	}
	
	public Genre find(int id) {
		return (Genre) super.find(Genre.class, id);
	}
	
	public List findAll() {
		return super.findAll("genres");
	}
	
	public Genre getByName(String name) {
		List<Genre> all = findAll();
		for (Genre a: all) {
			if (a.getName().equals(name)) {
				return a;
			}
		}
		return null;
	}
}
