package dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import bookstore.OrderBook;
import bookstore.OrderBookId;

public class OrderBookDao extends AbstractDao {
	public OrderBookDao() {
		super();
	}
	
	public void saveOrUpdate(OrderBook orderBook) {
		super.saveOrUpdate(orderBook);
	}
	
	public void delete(OrderBook orderBook) {
		super.delete(orderBook);
	}
	
	public List<OrderBook> find(int id) {
		start();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<OrderBook> orderBookQuery = builder.createQuery(OrderBook.class);
		Root<OrderBook> from = orderBookQuery.from(OrderBook.class);
		Join<OrderBook, OrderBookId> oIDJoin = from.join("pk");
		orderBookQuery.select(from).where(builder.equal(oIDJoin.get("order"), id));
		TypedQuery<OrderBook> tq = session.createQuery(orderBookQuery);
		transaction.commit();
		//closeSession();
		return tq.getResultList();
	}
	
	public List findAll() {
		return super.findAll("orders");
	}
}