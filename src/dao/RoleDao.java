package dao;

import java.util.List;

import bookstore.CoverType;
import bookstore.Role;

public class RoleDao extends AbstractDao {
	public RoleDao() {
		super();
	}
	
	public void saveOrUpdate(Role role) {
		super.saveOrUpdate(role);
	}
	
	public void delete(Role role) {
		super.delete(role);
	}
	
	public Role find(int id) {
		return (Role) super.find(Role.class, id);
	}
	
	public List findAll() {
		return super.findAll("roles");
	}
	
	public Role getByName(String name) {
		List<Role> all = findAll();
		for (Role a: all) {
			if (a.getName().equals(name)) {
				return a;
			}
		}
		return null;
	}
}
