package dao;

import java.util.List;

import bookstore.Author;
import bookstore.CoverType;

public class AuthorDao extends AbstractDao {
	public AuthorDao() {
		super();
	}
	
	public void saveOrUpdate(Author author) {
		super.saveOrUpdate(author);
	}
	
	public void delete(Author author) {
		super.delete(author);
	}
	
	public Author find(int id) {
		return (Author) super.find(Author.class, id);
	}
	
	public List findAll() {
		return super.findAll("authors");
	}
	
	public Author getByName(String name) {
		List<Author> all = findAll();
		for (Author a: all) {
			if (a.getName().equals(name)) {
				return a;
			}
		}
		return null;
	}
}
