package dao;

import java.util.LinkedList;
import java.util.List;

import bookstore.Book;
import bookstore.CoverType;
import bookstore.Genre;
import bookstore.Publisher;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class BookDao extends AbstractDao {
	public BookDao() {
		super();
	}
	
	public void saveOrUpdate(Book book) {
		super.saveOrUpdate(book);
	}
	
	public void delete(Book book) {
		super.delete(book);
	}
	
	public Book find(int id) {
		return (Book) super.find(Book.class, id);
	}
	
	public List findAll() {
		return super.findAll("books");
	}
	
	public BookFinder getFinder() {
		return new BookFinder();
	}
	
	public class BookFinder {
		private List<String> names = new LinkedList<String>();
		private List<String> genres = new LinkedList<String>();
		private List<String> publishers = new LinkedList<String>();
		private List<String> covertypes = new LinkedList<String>();
		private Integer maxPrice = null;
		private Integer minPrice = null;
		private Predicate namePred;
		private Predicate genrePred;
		private Predicate publisherPred;
		private Predicate covertypePred;
		private Predicate maxPricePred;
		private Predicate minPricePred;
		
		public BookFinder setMaxPrice(int price) {
			maxPrice = price;
			return this;
		}
		public BookFinder setMinPrice(int price) {
			minPrice = price;
			return this;
		}
		public BookFinder addName(String name) {
			names.add(name);
			return this;
		}
		public BookFinder addGenre(String genre) {
			genres.add(genre);
			return this;
		}
		public BookFinder addPublisher(String publisher) {
			publishers.add(publisher);
			return this;
		}
		public BookFinder addCovertype(String covertype) {
			covertypes.add(covertype);
			return this;
		}
		public List<Book> find() {
			start();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Book> bookQuery = builder.createQuery(Book.class);
			Root<Book> from = bookQuery.from(Book.class);
			Join<Book, Genre> genreJoin = from.join("genre");
			Join<Book, Publisher> publisherJoin = from.join("publisher");
			Join<Book, CoverType> covertypeJoin = from.join("covertype");
			namePred = (names.isEmpty()) ? builder.and() : builder.or();
			for (String a: names) {
				namePred = builder.or(namePred, builder.like(from.get("name"), a));
			}
			genrePred = createPred(genres, genrePred, genreJoin, builder);
			publisherPred = createPred(publishers, publisherPred, publisherJoin, builder);
			covertypePred = createPred(covertypes, covertypePred, covertypeJoin, builder);
			maxPricePred = (maxPrice == null) ? builder.and() : builder.le(from.get("price"), maxPrice);
			minPricePred = (minPrice == null) ? builder.and() : builder.ge(from.get("price"), minPrice);
			Predicate all = builder.and(namePred, genrePred, publisherPred, covertypePred, maxPricePred, minPricePred);
			bookQuery.select(from).where(all);
			TypedQuery<Book> tq = session.createQuery(bookQuery);
			transaction.commit();
			//closeSession();
			return tq.getResultList();
		}
		
		private <T> Predicate createPred(List<String> list, Predicate pred, Join<Book, T> join, CriteriaBuilder builder) {
			if (list.isEmpty()) {
				pred = builder.and();
			} else {
				pred = builder.or();
				for (String a: list) {
					pred = builder.or(pred, builder.equal(join.get("name"), a));
				}
			}
			return pred;
		}
	}
}
