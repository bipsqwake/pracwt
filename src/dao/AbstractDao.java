package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import hibernate.HibernateFactory;

public abstract class AbstractDao {
	protected Session session;
	protected Transaction transaction;
	
	public AbstractDao() {
		HibernateFactory.getSessionFactory();
		session = HibernateFactory.openSession(); 
	}
	
	protected void saveOrUpdate(Object obj) {
		try {
			start();
			session.saveOrUpdate(obj);
			transaction.commit();
		} catch (HibernateException e) {
			HibernateFactory.rollback(transaction);
			//e.printStackTrace();
			throw e;
		} finally {
			//HibernateFactory.close();
		}
	}
	
	protected void delete(Object obj) {
		try {
			start();
			session.delete(obj);
			transaction.commit();
		} catch (HibernateException e) {
			HibernateFactory.rollback(transaction);
			//e.printStackTrace();
			throw e;
		} finally {
			//HibernateFactory.close();
		}
	}
	
	protected Object find(Class class_, int id) {
		Object result = null;
		try {
			start();
			result = session.load(class_, id);
			transaction.commit();
		} catch (HibernateException e) {
			HibernateFactory.rollback(transaction);
			//e.printStackTrace();
			throw e;
		} finally {
			//HibernateFactory.close();
		}
		return result;
	}
	
	protected List findAll(String dbName) {
		List result = null;
		try {
			start();
			result = session.createQuery("FROM " + dbName).list();
			transaction.commit();
		} catch (HibernateException e) {
			HibernateFactory.rollback(transaction);
			//e.printStackTrace();
			throw e;
		} finally {
			//HibernateFactory.close();
		}
		return result;
	}
	
	protected void start() {
		session = HibernateFactory.openSession();
		transaction = session.beginTransaction();
	}
	
	public void closeSession() {
		HibernateFactory.close();
	}
}
