package dao;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import bookstore.Order;
import bookstore.OrderCondition;

public class OrderDao extends AbstractDao {
	public OrderDao() {
		super();
	}
	
	public void saveOrUpdate(Order order) {
		super.saveOrUpdate(order);
	}
	
	public void delete(Order order) {
		super.delete(order);
	}
	
	public Order find(int id) {
		return (Order) super.find(Order.class, id);
	}
	
	public List findAll() {
		return super.findAll("ordersattach");
	}
	
	public OrderFinder getFinder() {
		return new OrderFinder();
	}
	
	public class OrderFinder {
		private List<String> conditions = new LinkedList<>();
		public OrderFinder addCondition(String condition) {
			conditions.add(condition);
			return this;
		}
		
		public List<Order> find() {
			start();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Order> orderQuery = builder.createQuery(Order.class);
			Root<Order> from = orderQuery.from(Order.class);
			Join<Order, OrderCondition> orderConditionJoin =  from.join("cond");
			Predicate condPredicate = (conditions.isEmpty()) ? builder.and() : builder.or();
			for (String a: conditions) {
				condPredicate = builder.or(condPredicate, builder.like(orderConditionJoin.get("name"), a));
			}
			orderQuery.select(from).where(condPredicate);
			TypedQuery<Order> tq = session.createQuery(orderQuery);
			transaction.commit();
			//closeSession();
			return tq.getResultList();
		}
	}
}
