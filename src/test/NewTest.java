package test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import bookstore.*;
import dao.BookDao.BookFinder;
import dao.CustomerDao.CustomerFinder;
import dao.OrderDao.OrderFinder;
import hibernate.HibernateFactory;

public class NewTest {
	BookStore bookstore = new BookStore();
	@Test(description = "�������� ����")
	public void Test01() {
		System.out.println("FirstTest");
		Book book = bookstore.getBookDao().find(1);
		assertEquals(book.getName(), "�� �������� ���, �������");
		assertEquals(book.getCovertype().getName(), "������");
		assertEquals(book.getGenre().getName(), "�����");
		assertEquals(book.getPages(), 544);
		assertEquals(book.getPrice(), 498);
		assertEquals(book.getPublisher().getName(), "������-�������");
		assertEquals(book.getStock(), 20);
		for (Author a: book.getAuthors()) {
			assertEquals(a.getName(), "���� ������"); 
		}
	}
	@Test(description = "����� \"������� ������\" ����� ���� �������", dependsOnMethods={"Test01"})
	public void Test02() {
		List<Book> books = bookstore.getBookDao().findAll();
		boolean find = false;
		for (Book a: books) {
			if (a.getName().equals("������� ������")) {
				assertEquals(a.getAuthors().size(), 2);
				find = true;
			}
		}
		assertTrue(find);
	}
	@Test(description = "����� \"������� ������ 2\" �����������", dependsOnMethods={"Test02"})
	public void Test03() {
		List<Book> books = bookstore.getBookDao().findAll();
		boolean find = false;
		for (Book a: books) {
			if (a.getName().equals("������� ������ 2")) {
				find = true;
			}
		}
		assertFalse(find);
	}
	@Test(description = "���������� ����� �����", dependsOnMethods={"Test03"})
	public void Test04() {
		Book book = new Book();
		book.setName("����������� ������� ������");
		book.setGenre(bookstore.getGenreDao().getByName("�����"));
		book.setPublisher(bookstore.getPublisherDao().getByName("���"));
		book.setCovertype(bookstore.getCoverTypeDao().getByName("������"));
		book.setPages(346);
		book.setPrice(199);
		book.setStock(13);
		book.setYear(2014);
		Author author = bookstore.getAuthordao().getByName("������ �������");
		book.addAuthor(author);
		author.addBook(book);
		bookstore.getBookDao().saveOrUpdate(book);
		Author author2 = bookstore.getAuthordao().getByName("������ �������");
		boolean find = false;
		for (Book a: author2.getBooks()) {
			if (a.getName().equals("����������� ������� ������")) {
				find = true;
			}
		}
		assertTrue(find);
	}
	@Test(description = "���������� ������ ������ �����", dependsOnMethods={"Test04"})
	public void Test05() {
		Book book = null;
		for (Book a: (List<Book>)bookstore.getBookDao().findAll()) {
			if (a.getName().equals("������� ���")) {
				book = a;
				break;
			}
		}
		assertNotEquals(null, book);
		int numOfAuthors = book.getAuthors().size();
		book.addAuthor(bookstore.getAuthordao().getByName("���� ������"));
		bookstore.getAuthordao().getByName("���� ������").addBook(book);
		bookstore.getBookDao().saveOrUpdate(book);
		Book book2 = null;
		for (Book a: (List<Book>)bookstore.getBookDao().findAll()) {
			if (a.getName().equals("������� ���")) {
				book2 = a;
				break;
			}
		}
		assertNotEquals(book2, null);
		System.out.println(numOfAuthors + " " +book2.getAuthors().size());
		assertEquals(numOfAuthors + 1, book2.getAuthors().size());
	}
	@Test(description = "3 ����� � ������ �������", dependsOnMethods={"Test05"})
	public void Test06() {
		BookFinder finder = bookstore.getBookDao().getFinder();
		List<Book> books = finder.addCovertype("������").find();
		assertEquals(books.size(), 3);
	}
	@Test(description = "���������� ������������ �������, ��������, ���������� ����� � �� ����������", dependsOnMethods={"Test06"})
	public void Test07() {
		OrderFinder finder = bookstore.getOrderDao().getFinder();
		finder.addCondition("����������");
		List<Order> orders = finder.find();
		assertEquals(1, orders.size());
		Order order = orders.get(0);
		Customer customer = order.getCustomer();
		assertEquals("�������", customer.getFirstname());
		assertEquals("�������", customer.getSecondname());
		Set<OrderBook> orderBooks = order.getOrderBooks();
		assertEquals(1, orderBooks.size());
		OrderBook orderBook = orderBooks.toArray(new OrderBook[1])[0];
		assertEquals(1, orderBook.getNum());
		Book book = orderBook.getBook();
		assertEquals("�����, ������", book.getName());
	}
	@Test(description = "���������� � �������� ������������", dependsOnMethods={"Test07"})
	public void Test08() {
		Customer customer = new Customer();
		customer.setFirstname("����");
		customer.setSecondname("��������");
		customer.setEmail("testtest@test.ru");
		customer.setPhone("+92563665284");
		customer.setAddress("����, �������� ����� �63");
		customer.setLogin("test");
		customer.setPassword("tset");
		customer.setRole(bookstore.getRoleDao().getByName("������������"));
		bookstore.getCustomerDao().saveOrUpdate(customer);
		CustomerFinder finder = bookstore.getCustomerDao().getFinder();
		finder.setFirstname("����");
		List<Customer> customers = finder.find();
		assertEquals(customers.size(), 1);
		Customer customer2 = bookstore.getCustomerDao().find(6);
		assertEquals(customer, customer2);
		bookstore.getCustomerDao().delete(customer2);
		customers = finder.find();
		assertEquals(customers.size(), 0);
	}
	@Test(description = "���������� �����", dependsOnMethods={"Test08"})
	public void Test09() {
		Author author1 = bookstore.getAuthordao().getByName("������ �������");
		int di�kensBook = author1.getBooks().size();
		Book book = new Book();
		book.setName("������� ����������� �����");
		book.setGenre(bookstore.getGenreDao().getByName("�����"));
		book.setCovertype(bookstore.getCoverTypeDao().getByName("������"));
		book.setPublisher(bookstore.getPublisherDao().getByName("���"));
		book.setYear(2012);
		book.setPages(568);
		book.setPrice(599);
		book.setStock(30);
		book.addAuthor(author1);
		author1.addBook(book);
		bookstore.getBookDao().saveOrUpdate(book);
		Author author2 = bookstore.getAuthordao().getByName("������ �������");
		assertEquals(author2.getBooks().size(), di�kensBook + 1);
		boolean find = false;
		for (Book a: author2.getBooks()) {
			if (a.getName() == "������� ����������� �����") {
				find = true;
			}
		}
		assertTrue(find);
	}
	@Test(description = "���������� �����", dependsOnMethods={"Test09"})
	public void Test10() {
		Book toUpdate = bookstore.getBookDao().find(1);
		int oldPrice = toUpdate.getPrice();
		String name = toUpdate.getName();
		System.out.println(name);
		toUpdate.setPrice(oldPrice + 50);
		bookstore.getBookDao().saveOrUpdate(toUpdate);
		BookFinder finder= bookstore.getBookDao().getFinder();
		List<Book> books = finder.addName(name).find();
		assertEquals(books.size(), 1);
		Book book = books.get(0);
		assertEquals(book.getPrice(), oldPrice + 50);
	}
	@Test(description = "�������� �����", dependsOnMethods={"Test10"})
	public void Test11() {
		Author author1 = bookstore.getAuthordao().getByName("���� ����");
		Author author2 = bookstore.getAuthordao().getByName("������� ������");
		int oldIlf = author1.getBooks().size();
		int oldPetrov = author2.getBooks().size();
		BookFinder finder = bookstore.getBookDao().getFinder();
		List<Book> books = finder.addName("������� ������").find();
		assertEquals(books.size(), 1);
		Book book = books.get(0);
		assertEquals(book.getName(), "������� ������");
		assertEquals(book.getCovertype().getName(), "������");
		Set<Author> authors = book.getAuthors();
		for (Author a: authors) {
			a.removeBook(book);
		}
		bookstore.getBookDao().delete(book);
		books = finder.find();
		assertEquals(books.size(), 0);
		author1 = bookstore.getAuthordao().getByName("���� ����");
		author2 = bookstore.getAuthordao().getByName("������� ������");
		assertEquals(author1.getBooks().size(), oldIlf - 1);
		assertEquals(author2.getBooks().size(), oldPetrov - 1);
		List<Book> books2 = bookstore.getBookDao().getFinder().addName("���������� �������").find();
		assertEquals(books2.size(), 1);
	}
	@Test(description = "�������� ������", dependsOnMethods={"Test11"})
	public void Test12() {
		Author author = bookstore.getAuthordao().getByName("���� ������");
		bookstore.getAuthordao().delete(author);
		Author author2 = bookstore.getAuthordao().getByName("���� ������");
		assertEquals(author2, null);		
	}
	@Test(description = "���������� ������", dependsOnMethods={"Test12"})
	public void Test13() {
		OrderFinder finder = bookstore.getOrderDao().getFinder();
		finder.addCondition("����������");
		List<Order> orders = finder.find();
		int ordersNum = orders.size();
		Customer customer = bookstore.getCustomerDao().find(1);
		Order order = new Order();
		order.setCond(bookstore.getOrderConditionDao().getByName("����������"));
		order.setCustomer(customer);
		Calendar cal = new GregorianCalendar();
		order.setTime(cal.getTime());
		customer.addOrder(order);
		bookstore.getOrderDao().saveOrUpdate(order);
		for (Book a : bookstore.getBookDao().getFinder().addGenre("����").find()) {
			OrderBook orderBook = new OrderBook();
			orderBook.setBook(a);
			orderBook.setNum(1);
			orderBook.setOrder(order);
			order.addOrderBook(orderBook);
			bookstore.getOrderBookDao().saveOrUpdate(orderBook);
		}
		orders = bookstore.getOrderDao().getFinder().addCondition("����������").find();
		assertEquals(orders.size(), ordersNum + 1);
		boolean chairs = false;
		for (Order a: orders) {
			if (a.getCustomer().getId() == customer.getId()) {
				for (OrderBook b: a.getOrderBooks()) {
					if (b.getBook().getName().equals("���������� �������")) {
						chairs = true; 
					} else {
						assertFalse(true);
					}
				}
			}
		}
		assertTrue(chairs);
	}
	@Test(description = "����� ��������� ������", dependsOnMethods={"Test13"})
	public void Test14() {
		OrderFinder finder1 = bookstore.getOrderDao().getFinder();
		int keepOrders = finder1.addCondition("����������").find().size();
		OrderFinder finder2 = bookstore.getOrderDao().getFinder();
		int keptOrders = finder2.addCondition("� ���������").find().size();
		Customer customer = bookstore.getCustomerDao().find(1);
		Set<Order> orders = customer.getOrders();
		assertEquals(orders.size(), 1);
		Order order = orders.toArray(new Order[1])[0];
		order.setCond(bookstore.getOrderConditionDao().getByName("� ���������"));
		bookstore.getOrderDao().saveOrUpdate(order);
		int newKeepOrders = finder1.find().size();
		int newKeptOrders = finder2.find().size();
		assertEquals(newKeepOrders, keepOrders - 1);
		assertEquals(newKeptOrders, keptOrders + 1);
	}
	@Test(description="��������� ������ ��������������� � �������������", dependsOnMethods={"Test14"})
	public void Test15()
	{
		CustomerFinder finder = bookstore.getCustomerDao().getFinder();
		List<Customer> admins = finder.setRole("�������������").find();
		assertEquals(admins.size(), 1);
		assertEquals(admins.get(0).getFirstname(), "������");
		CustomerFinder finder2 = bookstore.getCustomerDao().getFinder();
		List<Customer> users = finder2.setRole("������������").find();
		assertEquals(users.size(), 4);
		assertEquals(users.get(0).getFirstname(), "����-�����");
	}
	@Test(description="�������� ������", dependsOnMethods={"Test15"})
	public void Test16() 
	{
		int ordersNum = bookstore.getOrderDao().findAll().size();
		Order order = bookstore.getOrderDao().find(4);
		List<OrderBook> orderBooks = bookstore.getOrderBookDao().find(4);
		assertEquals(orderBooks.size(), 1);
		bookstore.getOrderDao().delete(order);
		List<OrderBook> orderBooks2 = bookstore.getOrderBookDao().find(4);
		assertEquals(orderBooks2.size(), 0);
		assertEquals(ordersNum - 1, bookstore.getOrderDao().findAll().size());
	}
	@Test(description="����������� ������ \"��������\" DAO-�������", dependsOnMethods={"Test16"})
	public void Test17()
	{
		assertEquals(bookstore.getOrderDao().findAll().size(), 3);
		Order order = bookstore.getOrderDao().find(1);
		assertEquals(order.getCond().getName(), "� ���������");
		assertEquals(order.getCustomer().getFirstname(), "����-�����");
		assertEquals(bookstore.getOrderBookDao().find(order.getId()).size(), 2);
		Order order2 = bookstore.getOrderDao().find(2);
		assertEquals(order2.getCond().getName(), "����������");
		assertEquals(order2.getCustomer().getFirstname(), "�������");
		assertEquals(bookstore.getOrderBookDao().find(order2.getId()).size(), 1);
		
		assertEquals(bookstore.getOrderConditionDao().findAll().size(), 4);
		assertEquals(bookstore.getAuthordao().findAll().size(), 11);
		assertEquals(bookstore.getCoverTypeDao().findAll().size(), 2);
		assertEquals(bookstore.getGenreDao().findAll().size(), 4);
		assertEquals(bookstore.getPublisherDao().findAll().size(), 5);
		assertEquals(bookstore.getRoleDao().findAll().size(), 2);
		assertEquals(bookstore.getRoleDao().find(1).getName(), "������������");
		assertEquals(bookstore.getPublisherDao().find(4).getName(), "�");
		assertEquals(bookstore.getGenreDao().find(1).getName(), "�����");
		assertEquals(bookstore.getAuthordao().find(3).getName(), "��� �������");
		assertEquals(bookstore.getOrderConditionDao().find(3).getName(), "�����������");
		
		assertEquals(bookstore.getCustomerDao().findAll().size(), 5);
		
		Genre genre = new Genre();
		genre.setName("�����");
		bookstore.getGenreDao().saveOrUpdate(genre);
		assertEquals(bookstore.getGenreDao().findAll().size(), 5);
		assertEquals(bookstore.getGenreDao().find(5).getName(), "�����");
		
		
		Role role = bookstore.getRoleDao().find(1);
		role.setName("����");
		bookstore.getRoleDao().saveOrUpdate(role);
		assertEquals(bookstore.getRoleDao().findAll().size(), 2);
		assertEquals(bookstore.getRoleDao().find(1).getName(), "����");
		assertEquals(bookstore.getRoleDao().find(2).getName(), "�������������");   
	}
	@AfterMethod
	public void ex() {
		HibernateFactory.close();
	}
	@AfterTest
	public void quit() {
		HibernateFactory.closeFactory();
	}
}
