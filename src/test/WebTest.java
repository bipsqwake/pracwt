package test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;


public class WebTest {
	public String baseUrl = "http://localhost:8080/prac123/";
    public WebDriver driver ; 
    
    @BeforeMethod
    public void beforeMethod() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(2, TimeUnit.SECONDS);
        driver.get("http://localhost:8080/prac123/");
    }
    
    @AfterMethod
    public void afterMethod() {
    	driver.close();
    }
    
    @Test (description = "Find book by name ")
    public void test01() {
    	WebDriverWait wait = new WebDriverWait(driver,10);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("name")));
    	driver.findElement(By.name("name")).sendKeys("Go set a watchman");
    	driver.findElement(By.name("submit")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("bookshelf")));
    	String a = driver.findElement(By.xpath("//*[@id='bookshelf']/tbody/tr/td")).getText();
    	assertEquals(a, "Go set a watchman");
    	List<WebElement> rows = driver.findElements(By.xpath("//*[@id='bookshelf']/tbody/tr")); 
    	assertEquals(rows.size(), 1);
    }
    
    @Test(description = "Find book by publisher AST and max price")
    public void test03() {
    	WebDriverWait wait = new WebDriverWait(driver,10);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("name")));
    	driver.findElement(By.name("publisher")).sendKeys("AST");
    	driver.findElement(By.name("maxPrice")).sendKeys("300");
    	driver.findElement(By.name("submit")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("bookshelf")));
    	List<WebElement> rows = driver.findElements(By.xpath("//*[@id='bookshelf']/tbody/tr")); 
    	assertEquals(rows.size(), 5);
    }
    
    @Test(description = "Correct sign in")
    public void test04() {
    	WebDriverWait wait = new WebDriverWait(driver,10);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login")));
    	driver.findElement(By.id("login")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("signup")));
    	driver.findElement(By.id("signup")).click();
    	driver.findElement(By.id("firstname")).sendKeys("Imya");
    	driver.findElement(By.id("secondname")).sendKeys("Familiya");
    	driver.findElement(By.id("login")).sendKeys("login");
    	driver.findElement(By.id("password")).sendKeys("112233");
    	driver.findElement(By.id("email")).sendKeys("newemail@yanex.ru");
    	driver.findElement(By.id("phone")).sendKeys("+897593232");
    	driver.findElement(By.id("address")).sendKeys("Ulitsa addressnaya");
    	driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("signmes")));
    	String a = driver.findElement(By.id("signmes")).getText();
    	assertEquals(a, "Sucssesfully signuped");
    }
    
    @Test(description = "Incorrect signin")
    public void test05() {
    	WebDriverWait wait = new WebDriverWait(driver,10);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login")));
    	driver.findElement(By.id("login")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("signup")));
    	driver.findElement(By.id("signup")).click();
    	driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("errorMes")));
    	String a = driver.findElement(By.id("errorMes")).getText();
    	assertEquals(a, "Firstname can't be empty");
    	driver.findElement(By.id("firstname")).sendKeys("Imya");
    	driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("errorMes")));
    	a = driver.findElement(By.id("errorMes")).getText();
    	assertEquals(a, "Secondname can't be empty");
    	driver.findElement(By.id("secondname")).sendKeys("Familiya");
    	driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("errorMes")));
    	a = driver.findElement(By.id("errorMes")).getText();
    	assertEquals(a, "Login can't be empty");
    	driver.findElement(By.id("login")).sendKeys("login");
    	driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("errorMes")));
    	a = driver.findElement(By.id("errorMes")).getText();
    	assertEquals(a, "Password can't be empty");
    	driver.findElement(By.id("password")).sendKeys("112233");
    	driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("errorMes")));
    	a = driver.findElement(By.id("errorMes")).getText();
    	assertEquals(a, "Email can't be empty");
    	driver.findElement(By.id("email")).sendKeys("newemail@yanex.ru");
    	driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("errorMes")));
    	a = driver.findElement(By.id("errorMes")).getText();
    	assertEquals(a, "Phone can't be empty");
    	driver.findElement(By.id("phone")).sendKeys("+897593232");
    	driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("errorMes")));
    	a = driver.findElement(By.id("errorMes")).getText();
    	assertEquals(a, "Address can't be empty");
    	driver.findElement(By.id("address")).sendKeys("Ulitsa addressnaya");
    	driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("errorMes")));
    	a = driver.findElement(By.id("errorMes")).getText();
    	
    	System.out.println("MID");
    	
    	assertEquals(a, "That username already registered");
    	driver.findElement(By.id("login")).clear();
    	driver.findElement(By.id("login")).sendKeys("newlogin");
    	driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("errorMes")));
    	a = driver.findElement(By.id("errorMes")).getText();
    	assertEquals(a, "That email occupied");
    	driver.findElement(By.id("email")).sendKeys("newemail@yandex.ru");
    	driver.findElement(By.id("email")).clear();
    	driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("errorMes")));
    	a = driver.findElement(By.id("errorMes")).getText();
    	assertEquals(a, "That phone occupied");
    	driver.findElement(By.id("email")).clear();
    	driver.findElement(By.id("phone")).sendKeys("+67847348934");
    	driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
    	
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("signmes")));
    	a = driver.findElement(By.id("signmes")).getText();
    	assertEquals(a, "Sucssesfully signuped");
    }
    
    @Test(description = "Incorrect login")
    public void test06() {
    	WebDriverWait wait = new WebDriverWait(driver,10);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login")));
    	driver.findElement(By.id("login")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login")));
    	driver.findElement(By.id("login")).sendKeys("hehehehe");
    	driver.findElement(By.id("password")).sendKeys("112233");
    	driver.findElement(By.id("loginbutton")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("errorMes")));
    	String a = driver.findElement(By.id("errorMes")).getText();
    	assertEquals(a, "No such user");
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login")));
    	driver.findElement(By.id("login")).clear();
    	driver.findElement(By.id("login")).sendKeys("hehehe");
    	driver.findElement(By.id("loginbutton")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("errorMes")));
    	a = driver.findElement(By.id("errorMes")).getText();
    	assertEquals(a, "Wrong login or password");
    }
    
    @Test(description = "Correct login")
    public void test07() {
    	WebDriverWait wait = new WebDriverWait(driver,10);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login")));
    	driver.findElement(By.id("login")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login")));
    	driver.findElement(By.id("login")).sendKeys("hehehe");
    	driver.findElement(By.id("password")).sendKeys("StarWars");
    	driver.findElement(By.id("loginbutton")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcomeMes")));
    	String a = driver.findElement(By.id("welcomeMes")).getText();
    	assertEquals(a, "Welcome, Griffin Peter");
    }
    
    @Test(description = "Ordering books")
    public void test08() {
    	WebDriverWait wait = new WebDriverWait(driver,5, 1000);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login")));
    	driver.findElement(By.id("login")).click();
    	wait.until(ExpectedConditions.presenceOfElementLocated(By.id("password")));
    	driver.findElement(By.id("login")).sendKeys("hehehe");
    	driver.findElement(By.id("password")).sendKeys("StarWars");
    	driver.findElement(By.id("loginbutton")).click();
    	wait.until(ExpectedConditions.presenceOfElementLocated(By.id("welcomeMes")));
    	String a = driver.findElement(By.id("welcomeMes")).getText();
    	assertEquals(a, "Welcome, Griffin Peter");
    	assertEquals(driver.findElement(By.id("cartMes")).getText(), "In your cart 0 items");
    	assertEquals(driver.findElement(By.xpath("//table[@id='bookshelf']/tbody/tr[2]/td[7]")).getText(), "20");
    	assertEquals(driver.findElement(By.xpath("//table[@id='bookshelf']/tbody/tr[3]/td[7]")).getText(), "30");
    	driver.findElement(By.xpath("//table[@id='bookshelf']/tbody/tr[2]/td[8]")).click();
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcomeMes")));
    	driver.findElement(By.xpath("//table[@id='bookshelf']/tbody/tr[3]/td[8]")).click();
    	wait.until(ExpectedConditions.presenceOfElementLocated(By.id("welcomeMes")));
    	assertEquals(driver.findElement(By.id("cartMes")).getText(), "In your cart 2 items");
    	assertEquals(driver.findElement(By.xpath("//table[@id='bookshelf']/tbody/tr[2]/td[7]")).getText(), "19");
    	assertEquals(driver.findElement(By.xpath("//table[@id='bookshelf']/tbody/tr[3]/td[7]")).getText(), "29");
    	driver.findElement(By.linkText("cart")).click();
    	driver.findElement(By.linkText("csdfsdfsdfart")).click();
    	
    	
    }
    
    
}
