<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<a href="user.do?userId=${user.id}">Back</a>
<c:if test="${not empty order}">
<p align="center">Cart</p>
<table border="1" align="center">
    <tr><td>Condition: ${order.cond.name}</td></tr>
    <tr><td>Time: ${order.time}</td></tr>
    <c:forEach var="orderbook" items="${order.orderBooks}">
    <tr><td>${orderbook.book.name}</td><td>${orderbook.book.price} rub</td><td>${orderbook.num} pcs.</td>
    <td><form method="POST" action="removebook.do">
        <input type="hidden" name="userId" value="${user.id}">
        <input type="hidden" name="bookId" value="${orderbook.book.id}">
        <input type="submit" value="Remove"></form></td>
    </tr>
    </c:forEach>
    <tr><td>sum - ${sum} rub</td></tr>
    <tr><td><form method="POST" action="checkout.do">
        <input type="hidden" name="userId" value="${user.id}">
        <input type="submit" value="Checkout"></form></td>
    </tr>
    <tr><td><form method="POST" action="cancelorder.do">
        <input type="hidden" name="userId" value="${user.id}">
        <input type="submit" value="Cancel"></form></td>
    </tr>
</table>
</c:if>
<c:if test="${empty order}">
<p>Cart is empty</p>
</c:if>
<table align="center" width="90%" border="1">
<tr><td>Processing</td><td>Prepared</td><td>Completed</td></tr>
<tr>
<td>
<table>
    <c:forEach var="order" items="${procOrders}">
    <tr><td>
        <table border="1">
            <tr><td>Order num: ${order.id}</td></tr>
            <tr><td>Condition: ${order.cond.name}</td></tr>
            <tr><td>Time: ${order.time}</td></tr>
            <c:forEach var="orderbook" items="${order.orderBooks}">
            <tr><td>${orderbook.book.name}</td><td>${orderbook.book.price} rub</td><td>${orderbook.num} pcs.</td></tr>
            </c:forEach>
        </table>
        <br/>
    </td></tr>
    </c:forEach>
</table>
</td>
<td>
<table>
    <c:forEach var="order" items="${prepOrders}">
    <tr><td>
        <table border="1">
            <tr><td>Order num: ${order.id}</td></tr>
            <tr><td>Condition: ${order.cond.name}</td></tr>
            <tr><td>Time: ${order.time}</td></tr>
            <c:forEach var="orderbook" items="${order.orderBooks}">
            <tr><td>${orderbook.book.name}</td><td>${orderbook.book.price} rub</td><td>${orderbook.num} pcs.</td></tr>
            </c:forEach>
        </table>
        <br/>
    </td></tr>
    </c:forEach>
</table>
</td>
<td>
<table>
    <c:forEach var="order" items="${compOrders}">
    <tr><td>
        <table border="1">
            <tr><td>Order num: ${order.id}</td></tr>
            <tr><td>Condition: ${order.cond.name}</td></tr>
            <tr><td>Time: ${order.time}</td></tr>
            <c:forEach var="orderbook" items="${order.orderBooks}">
            <tr><td>${orderbook.book.name}</td><td>${orderbook.book.price} rub</td><td>${orderbook.num} pcs.</td></tr>
            </c:forEach>
        </table>
        <br/>
    </td></tr>
    </c:forEach>
</table>
</td>
</tr>
</table>
    