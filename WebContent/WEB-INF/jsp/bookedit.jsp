<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<td><a href="bookcontrol.do?userId=${user.id}">Back</a></td>
<table align="center" border="2">
    <tr><td><form method="POST" action="editbook.do">
        <input type="text" name="authorname">
        <input type="hidden" name="userId" value="${user.id}">
        <input type="hidden" name="bookId" value="${book.id}">
        <input type="submit" value="Add author" name="add"/></form></td></tr>
    <form:form modelAttribute="book" method="POST" action="savebook.do?userId=${user.id}&bookId=${book.id}">
    <tr><td>Name</td><td><form:input path="name" value="${name}" size="30" maxlength="80"/></td></tr>
    <tr><td>Publisher</td><td><form:input path="publisher.name" value="${publisher.name}" size="30" maxlength="80"/></td></tr>
    <tr><td>CoverType</td><td><form:input path="covertype.name" value="${covertype.name}" size="30" maxlength="80"/></td></tr>
    <tr><td>Genre</td><td><form:input path="genre.name" value="${genre.name}" size="30" maxlength="80"/></td></tr>
    <tr><td>Price</td><td><form:input path="price" value="${price}" size="30" maxlength="80"/></td></tr>
    <tr><td>Pages</td><td><form:input path="pages" value="${pages}" size="30" maxlength="80"/></td></tr>
    <tr><td>Year</td><td><form:input path="year" value="${year}" size="30" maxlength="80"/></td></tr>
    <tr><td>Stock</td><td><form:input path="stock" value="${stock}" size="30" maxlength="80"/></td></tr>
    <tr><td>Authors</td>
    <c:forEach var="author" items="${book.authors}">
    <td>${author.name}  <a href="removeauthor.do?bookId=${book.id}&authId=${author.id}&userId=${user.id}">Remove</a></td>
    </c:forEach>
    </tr>
    <td><p class="submit"><input type="submit" value="Save"/></p></td></tr>
    </form:form>
</table>