<%@ include file="/WEB-INF/jsp/includes.jsp" %>


<table align="center" border="1" id="userinfo">
    <form:form modelAttribute="user">
    <tr><td>First Name</td><td><form:input path="firstname" size="30" maxlength="80"/></td></tr>
    <tr><td>Second Name</td><td><form:input path="secondname" size="30" maxlength="80"/></td></tr>
    <tr><td>Login</td><td><form:input path="login" size="30" maxlength="80"/></td></tr>
    <tr><td>Password</td><td><form:input path="password" size="30" maxlength="80"/></td></tr>
    <tr><td>E-mail</td><td><form:input path="email" size="30" maxlength="80"/></td></tr>
    <tr><td>Phone</td><td><form:input path="phone" size="30" maxlength="80"/></td></tr>
    <tr><td>Address</td><td><form:input path="address" size="30" maxlength="80"/></td></tr>
    <tr><td><p class="submit"><input type="submit" value="Save"/></p></td></tr>
    </form:form>
</table>
<p align="center" id="errorMes">${errorMes}</p>