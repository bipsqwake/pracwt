<%@ include file="/WEB-INF/jsp/includes.jsp" %>
<a href="admin.do?userId=${admin.id}">Back</a>

<table align="center" border="2">
    <tr>
        <td>Login</td>
        <td>Password</td>
        <td>FirstName</td>
        <td>SecondName</td>
        <td>Email</td>
        <td>Phone</td>
        <td>Address</td>
        <td>Role</td>
    </tr>
    <c:forEach var="user" items="${users}">
    <tr>
        <td>${user.login}</td>
        <td>${user.password}</td>
        <td>${user.firstname}</td>
        <td>${user.secondname}</td>
        <td>${user.email}</td>
        <td>${user.phone}</td>
        <td>${user.address}</td>
        <td>${user.role.name}</td>
        <td><form method="POST" action="makeadmin.do">
            <input type="hidden" name="userId" value="${user.id}">
            <input type="hidden" name="adminId" value="${admin.id}">
            <input type="submit" value="Admin"></form></td>
        <td><form method="POST" action="disadmin.do">
            <input type="hidden" name="userId" value="${user.id}">
            <input type="hidden" name="adminId" value="${admin.id}">
            <input type="submit" value="Disadmin"></form></td>
    </tr>
    </c:forEach>
</table>
${errorMes}