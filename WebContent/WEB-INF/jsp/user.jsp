﻿<%@ include file="/WEB-INF/jsp/includes.jsp" %>
<a href="login.do?mes=%20">Back</a>
<p id="welcomeMes">Welcome, ${user.secondname} ${user.firstname}</p>
<p id = "cartMes">In your <a href="cart.do?userId=${user.id}">cart</a> ${booksnum} items</p>

<form method="POST" action="usersearch.do">
<table align="center">
    <tr><td>Name</td><td><input type="text" name="name"></td></tr>
    <tr><td>Publisher</td><td><input type="text" name="publisher"></td></tr>
    <tr><td>Genre</td><td><input type="text" name="genre"></td></tr>
    <tr><td>Max Price</td><td><input type="text" name="maxPrice"></td></tr>
    <tr><td>Min Price</td><td><input type="text" name="minPrice"></td></tr>
    <tr><td><input type="submit" name="submit" value="Search"></td></tr>
</table>
</form>
 
<table align="center" border="2" id="bookshelf">
    
    <tr>
        <td>Name</td>
        <td>Author</td>
        <td>Publisher</td>
        <td>Cover</td>
        <td>Genre</td>
        <td>Price</td>
        <td>Stock</td>
    </tr>
    <c:forEach var="book" items="${bookList}">
    <tr>
        <td>${book.name}</td>
        <td>
        <c:forEach var="author" items="${book.authors}">
            ${author.name} 
        </c:forEach>
        </td>
        <td>${book.publisher.name}</td>
        <td>${book.covertype.name}</td>
        <td>${book.genre.name}</td>
        <td>${book.price}</td>
        <td>${book.stock}</td>
        <td><a href="addtocart.do?bookId=${book.id}&userId=${user.id}">Add to cart</a></td>
    </tr>
    </c:forEach>
</table>