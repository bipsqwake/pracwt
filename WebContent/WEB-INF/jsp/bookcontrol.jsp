<%@ include file="/WEB-INF/jsp/includes.jsp" %>
<table>
<tr>
<td><a href="admin.do?userId=${user.id}">Back</a></td>
<td><form method="GET" action="editbook.do" id="addbook">
    <input type="hidden" name="userId" value="${user.id}">
    <input type="hidden" name="bookId" value="0">
    <input type="submit" value="Add book"></form></td>
</tr>
</table>
<form method="POST" action="adminsearch.do">
<table align="center">
    <tr><td>Name</td><td><input type="text" name="name"></td></tr>
    <tr><td>Publisher</td><td><input type="text" name="publisher"></td></tr>
    <tr><td>Genre</td><td><input type="text" name="genre"></td></tr>
    <tr><td>Max Price</td><td><input type="text" name="maxPrice"></td></tr>
    <tr><td>Min Price</td><td><input type="text" name="minPrice"></td></tr>
    <tr><td><input type="submit" name="submit" value="Search"></td></tr>
</table>
</form>
<table align="center" border="2" id="bookshelf">
    <tr>
        <td>Name</td>
        <td>Author</td>
        <td>Publisher</td>
        <td>Cover</td>
        <td>Genre</td>
        <td>Price</td>
        <td>Stock</td>
    </tr>
    <c:forEach var="book" items="${bookList}">
    <tr>
        <td>${book.name}</td>
        <td>
        <c:forEach var="author" items="${book.authors}">
            ${author.name} 
        </c:forEach>
        </td>
        <td>${book.publisher.name}</td>
        <td>${book.covertype.name}</td>
        <td>${book.genre.name}</td>
        <td>${book.price}</td>
        <td>${book.stock}</td>
        <td><a href="editbook.do?bookId=${book.id}&userId=${user.id}">Edit</a></td>
        <td><a href="deletebook.do?bookId=${book.id}&userId=${user.id}">Delete</a></td>
    </tr>
    </c:forEach>
</table>