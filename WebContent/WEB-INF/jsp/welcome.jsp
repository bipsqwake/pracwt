﻿<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<h2 align="center">Makson's BOOKSTORE</h2>
<a href="login.do?mes=%20"><p align="right" id="login">Log In</p></a>

<form method="POST" action="welcomesearch.do">
<table align="center">
    <tr><td>Name</td><td><input type="text" name="name"></td></tr>
    <tr><td>Publisher</td><td><input type="text" name="publisher"></td></tr>
    <tr><td>Genre</td><td><input type="text" name="genre"></td></tr>
    <tr><td>Max Price</td><td><input type="text" name="maxPrice"></td></tr>
    <tr><td>Min Price</td><td><input type="text" name="minPrice"></td></tr>
    <tr><td><input type="submit" name="submit" value="Search"></td></tr>
</table>
</form>

<table align="center" border="2" id = "bookshelf">
<thead>
    <tr>
        <td>Name</td>
        <td>Author</td>
        <td>Publisher</td>
        <td>Cover</td>
        <td>Genre</td>
        <td>Price</td>
        <td>Stock</td>
    </tr>
</thead>
    <c:forEach var="book" items="${bookList}">
    <tr>
        <td>${book.name}</td>
        <td>
        <c:forEach var="author" items="${book.authors}">
            ${author.name} 
        </c:forEach>
        </td>
        <td>${book.publisher.name}</td>
        <td>${book.covertype.name}</td>
        <td>${book.genre.name}</td>
        <td>${book.price}</td>
        <td>${book.stock}</td>
    </tr>
    </c:forEach>
</table>