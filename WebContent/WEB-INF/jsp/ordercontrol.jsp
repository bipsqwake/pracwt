<%@ include file="/WEB-INF/jsp/includes.jsp" %>
<table>
<tr>
<td><a href="admin.do?userId=${user.id}">Back</a></td>
<td><form method="POST" action="show.do">
<input type="hidden" name="userId" value="${user.id}">
<input type="hidden" name="condition" value="processing">
<input type="submit" value="Show processing"></form></td>
<td><form method="POST" action="show.do">
<input type="hidden" name="userId" value="${user.id}">
<input type="hidden" name="condition" value="prepared">
<input type="submit" value="Show prepared"></form></td>
<td><form method="POST" action="show.do">
<input type="hidden" name="userId" value="${user.id}">
<input type="hidden" name="condition" value="completed">
<input type="submit" value="Show completed"></form></td>
<td><form method="POST" action="show.do">
<input type="hidden" name="userId" value="${user.id}">
<input type="hidden" name="condition" value="all">
<input type="submit" value="Show all"></form></td>
<td><form method="POST" action="search.do">
<input type="hidden" name="userId" value="${user.id}">
<input type="text" name="search">
<input type="submit" value="Search"></form></td>
</tr>
</table>
<c:forEach var="order" items="${orderList}">
    <table align="center" border="2" width="50%">
    <tr><td>Order num: ${order.id}</td></tr>
    <tr><td>${order.customer.login}</td></tr>
    <tr><td>${order.customer.firstname} ${order.customer.secondname}</td></tr>
    <tr><td>${order.customer.address}</td></tr>
    <tr><td>${order.customer.phone}</td></tr>
    <c:forEach var="orderbook" items="${order.orderBooks}">
        <tr><td>${orderbook.book.name}</td><td>${orderbook.num} pcs.</td></tr>
    </c:forEach>
    <tr><td>${order.cond.name}</td></tr>
    <tr>
        <td><form method="POST" action="prepare.do">
            <input type="hidden" name="orderId" value="${order.id}">
            <input type="hidden" name="userId" value="${user.id}">
            <input type="submit" value="Prepare"></form></td>
        <td><form method="POST" action="complete.do">
            <input type="hidden" name="orderId" value="${order.id}">
            <input type="hidden" name="userId" value="${user.id}">
            <input type="submit" value="Complete"></form></td></tr>
    <tr>
        <td><form method="POST" action="cancel.do">
            <input type="hidden" name="orderId" value="${order.id}">
            <input type="hidden" name="userId" value="${user.id}">
            <input type="submit" value="Cancel"></form></td>
        <td><form method="POST" action="remove.do">
            <input type="hidden" name="orderId" value="${order.id}">
            <input type="hidden" name="userId" value="${user.id}">
            <input type="submit" value="Remove"></form></td>
    </tr>
    </table>
    <br/>
</c:forEach>