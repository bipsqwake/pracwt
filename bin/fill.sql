USE BookStore;

INSERT INTO OrdersCond (name) VALUES ('����������');
INSERT INTO OrdersCond (name) VALUES ('� ���������');
INSERT INTO OrdersCond (name) VALUES ('�����������');
INSERT INTO OrdersCond (name) VALUES ('��������');

INSERT INTO CoverType (name) VALUES ('������');
INSERT INTO CoverType (name) VALUES ('������');

INSERT INTO Genres (name) VALUES ('�����');
INSERT INTO Genres (name) VALUES ('������');
INSERT INTO Genres (name) VALUES ('��������');

INSERT INTO Publishers (name) VALUES ('���');
INSERT INTO Publishers (name) VALUES ('������');
INSERT INTO Publishers (name) VALUES ('������');
INSERT INTO Publishers (name) VALUES ('�');
INSERT INTO Publishers (name) VALUES ('������-�������');

INSERT INTO Roles (name) VALUES ('������������');
INSERT INTO Roles (name) VALUES ('�������������');

INSERT INTO Authors (name) VALUES ('���� ������');
INSERT INTO Authors (name) VALUES ('������ ��');
INSERT INTO Authors (name) VALUES ('��� �������');
INSERT INTO Authors (name) VALUES ('������ �����');
INSERT INTO Authors (name) VALUES ('������� ������');
INSERT INTO Authors (name) VALUES ('���� ����');
INSERT INTO Authors (name) VALUES ('������ �������');
INSERT INTO Authors (name) VALUES ('�������� ������');
INSERT INTO Authors (name) VALUES ('���� ������');
INSERT INTO Authors (name) VALUES ('������ �������');
INSERT INTO Authors (name) VALUES ('����� ���');
INSERT INTO Authors (name) VALUES ('����� ������');

INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('�� �������� ���, �������', 1, 5, 2015, 544, 2, 498, 20);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('����� ������� �������', 1, 1, 2105, 320, 2, 498, 30);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('����� ������������', 1, 1, 2016, 412, 2, 498, 30);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('��������', 1, 1, 2016, 544, 1, 199, 14);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('������� ���', 1, 4, 2016, 608, 1, 195, 6);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('���������� �������', 2, 2, 2016, 320, 1, 129, 14);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('������� ������', 2, 2, 2016, 425, 1, 129, 12);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('�������������� �������', 1, 5, 2010, 240, 1, 139, 13);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('����� ���', 1, 5, 2014, 544, 1, 155, 2);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('��������� ������� �������� ����', 1, 5, 2014, 352, 1, 109, 10);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('���������� ���', 1, 1, 2016, 317, 1, 149, 7);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('������ ������', 1, 1, 2016, 413, 1, 179, 17);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('�����, ������', 1, 1, 2016, 765, 1, 279, 14);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('������� ���������', 1, 1, 2016, 574, 1, 289, 8);

INSERT INTO AuthorsAttach (book, author) VALUES (1, 1);
INSERT INTO AuthorsAttach (book, author) VALUES (2, 2);
INSERT INTO AuthorsAttach (book, author) VALUES (3, 2);
INSERT INTO AuthorsAttach (book, author) VALUES (4, 3);
INSERT INTO AuthorsAttach (book, author) VALUES (5, 4);
INSERT INTO AuthorsAttach (book, author) VALUES (6, 5);
INSERT INTO AuthorsAttach (book, author) VALUES (6, 6);
INSERT INTO AuthorsAttach (book, author) VALUES (7, 5);
INSERT INTO AuthorsAttach (book, author) VALUES (7, 6);
INSERT INTO AuthorsAttach (book, author) VALUES (8, 7);
INSERT INTO AuthorsAttach (book, author) VALUES (9, 8);
INSERT INTO AuthorsAttach (book, author) VALUES (10, 9);
INSERT INTO AuthorsAttach (book, author) VALUES (11, 10);
INSERT INTO AuthorsAttach (book, author) VALUES (12, 11);
INSERT INTO AuthorsAttach (book, author) VALUES (13, 11);
INSERT INTO AuthorsAttach (book, author) VALUES (14, 12);

INSERT INTO Customers (firstname, secondname, email, phone, address, login, password, role)
VALUES ('������', '������', 'bipsqwake42@gmail.com', '+79255706708', '������������ �������� � 20�11', 'bipsqwake', 'qwerty', 2);
INSERT INTO Customers (firstname, secondname, email, phone, address, login, password)
VALUES ('����-�����', '����', 'glazastic@mail.ru', '+79256714509', '�������', 'mbird', 'Atticus');
INSERT INTO Customers (firstname, secondname, email, phone, address, login, password)
VALUES ('���������', '����', 'gklol@rambler.ru', '+79167459328', '�������� �������� �34', 'princessaVkedah', 'StarWars');
INSERT INTO Customers (firstname, secondname, email, phone, address, login, password)
VALUES ('�����', '������', 'greatcombinator@yandex.ru', '+79263551900', '��������� ������ �12', 'Bender12', '21redneB');
INSERT INTO Customers (firstname, secondname, email, phone, address, login, password)
VALUES ('�������', '�������', 'billionear@gmail.com', '+79057345189', '��������������� �������� �20', 'rofler1935', '123qwe');

INSERT INTO OrdersAttach (customer, cond, time)
VALUES (2, 2, '2017-4-14');
INSERT INTO OrdersAttach (customer, cond, time)
VALUES (5, 1, '2017-3-5');
INSERT INTO OrdersAttach (customer, cond, time)
VALUES (4, 3, '2017-2-27');

INSERT INTO Orders (id, book, num) VALUES (1, 2, 1);
INSERT INTO Orders (id, book, num) VALUES (1, 3, 1);
INSERT INTO Orders (id, book, num) VALUES (2, 13, 1);
INSERT INTO Orders (id, book, num) VALUES (3, 6, 1);
INSERT INTO Orders (id, book, num) VALUES (3, 7, 1);


