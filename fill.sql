USE BookStore;

INSERT INTO OrdersCond (name) VALUES ('assemble');
INSERT INTO OrdersCond (name) VALUES ('processing');
INSERT INTO OrdersCond (name) VALUES ('prepared');
INSERT INTO OrdersCond (name) VALUES ('completed');

INSERT INTO CoverType (name) VALUES ('soft');
INSERT INTO CoverType (name) VALUES ('hard');

INSERT INTO Genres (name) VALUES ('novel');
INSERT INTO Genres (name) VALUES ('poetry');
INSERT INTO Genres (name) VALUES ('detective');
INSERT INTO Genres (name) VALUES ('humor');

INSERT INTO Publishers (name) VALUES ('AST');
INSERT INTO Publishers (name) VALUES ('Martin');
INSERT INTO Publishers (name) VALUES ('Moskva');
INSERT INTO Publishers (name) VALUES ('A');
INSERT INTO Publishers (name) VALUES ('Azbuka-Attikus');

INSERT INTO Roles (name) VALUES ('user');
INSERT INTO Roles (name) VALUES ('administrator');

INSERT INTO Authors (name) VALUES ('Pierre Lemaitre');
INSERT INTO Authors (name) VALUES ('Harper Lee');
INSERT INTO Authors (name) VALUES ('Chuck Palahniuk');
INSERT INTO Authors (name) VALUES ('Markus Zusak');
INSERT INTO Authors (name) VALUES ('Evgeny Petrov');
INSERT INTO Authors (name) VALUES ('Ilya Ilf');
INSERT INTO Authors (name) VALUES ('Charles Dickens');
INSERT INTO Authors (name) VALUES ('Charlotte Bronte');
INSERT INTO Authors (name) VALUES ('Jack London');
INSERT INTO Authors (name) VALUES ('William Golding');
INSERT INTO Authors (name) VALUES ('Irwin Shaw');
INSERT INTO Authors (name) VALUES ('Pavel Sanaev');

INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('Au Revoir La-Haut', 1, 5, 2015, 544, 2, 498, 20);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('Go set a watchman', 1, 1, 2105, 320, 2, 498, 30);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('To kill a mockingbird', 1, 1, 2016, 412, 2, 498, 30);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('Ghosts', 1, 1, 2016, 544, 1, 199, 14);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('Book theif', 1, 4, 2016, 608, 1, 195, 6);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('Twelve chairs', 4, 2, 2016, 320, 1, 129, 14);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('Golden calf', 4, 2, 2016, 425, 1, 129, 12);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('Christmas books', 1, 5, 2010, 240, 1, 139, 13);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('Jane Eyre', 1, 5, 2014, 544, 1, 155, 2);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('The little lady of the big house', 1, 5, 2014, 352, 1, 109, 10);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('Lord of the Flies', 1, 1, 2016, 317, 1, 149, 7);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('Nightwork', 1, 1, 2016, 413, 1, 179, 17);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('Rich man, poor man', 1, 1, 2016, 765, 1, 279, 14);
INSERT INTO Books (name, genre, publisher, year, pages, covertype, price, stock) 
VALUES ('Chronicles of the Hollow', 1, 1, 2016, 574, 1, 289, 8);

INSERT INTO AuthorsAttach (book, author) VALUES (1, 1);
INSERT INTO AuthorsAttach (book, author) VALUES (2, 2);
INSERT INTO AuthorsAttach (book, author) VALUES (3, 2);
INSERT INTO AuthorsAttach (book, author) VALUES (4, 3);
INSERT INTO AuthorsAttach (book, author) VALUES (5, 4);
INSERT INTO AuthorsAttach (book, author) VALUES (6, 5);
INSERT INTO AuthorsAttach (book, author) VALUES (6, 6);
INSERT INTO AuthorsAttach (book, author) VALUES (7, 5);
INSERT INTO AuthorsAttach (book, author) VALUES (7, 6);
INSERT INTO AuthorsAttach (book, author) VALUES (8, 7);
INSERT INTO AuthorsAttach (book, author) VALUES (9, 8);
INSERT INTO AuthorsAttach (book, author) VALUES (10, 9);
INSERT INTO AuthorsAttach (book, author) VALUES (11, 10);
INSERT INTO AuthorsAttach (book, author) VALUES (12, 11);
INSERT INTO AuthorsAttach (book, author) VALUES (13, 11);
INSERT INTO AuthorsAttach (book, author) VALUES (14, 12);

INSERT INTO Customers (firstname, secondname, email, phone, address, login, password, role)
VALUES ('Maksim', 'Ivanov', 'bipsqwake42@gmail.com', '+79255706708', 'Lomonosovsky prospekt 20 11', 'bipsqwake', 'qwerty', 2);
INSERT INTO Customers (firstname, secondname, email, phone, address, login, password)
VALUES ('Jean-Louise', 'Finch', 'glazastic@mail.ru', '+79256714509', 'Maycomb', 'mbird', 'Atticus');
INSERT INTO Customers (firstname, secondname, email, phone, address, login, password)
VALUES ('Peter', 'Griffin', 'griffon@rambler.ru', '+79167459328', 'Griffinland', 'hehehe', 'StarWars');
INSERT INTO Customers (firstname, secondname, email, phone, address, login, password)
VALUES ('Ostap', 'Bender', 'greatcombinator@yandex.ru', '+79263551900', 'Lubyansky proezd', 'Bender12', '21redneB');
INSERT INTO Customers (firstname, secondname, email, phone, address, login, password)
VALUES ('Rudolph', 'Jordakh', 'billionear@gmail.com', '+79057345189', 'Krasnoarmeysky prospekt 20', 'rofler1935', '123qwe');

INSERT INTO OrdersAttach (customer, cond, time)
VALUES (2, 2, '2017-4-14');
INSERT INTO OrdersAttach (customer, cond, time)
VALUES (5, 1, '2017-3-5');
INSERT INTO OrdersAttach (customer, cond, time)
VALUES (4, 3, '2017-2-27');

INSERT INTO Orders (id, book, num) VALUES (1, 2, 1);
INSERT INTO Orders (id, book, num) VALUES (1, 3, 1);
INSERT INTO Orders (id, book, num) VALUES (2, 13, 1);
INSERT INTO Orders (id, book, num) VALUES (3, 6, 1);
INSERT INTO Orders (id, book, num) VALUES (3, 7, 1);


